#!/bin/bash

HOST=$1
PROJECT=$2
SESSION=$3
XNATID=$4
USER=$5
PASS=$6

OLD_IFS="$IFS"

echo  "Determine ALT_HOST to try to work around http vs https firewall issues.  Always try to use http when possible."
if [[ "$HOST" =~ ^https:.* ]] ; then
	ALT_HOST="$HOST"
	HOST=$(echo "$ALT_HOST" | sed -e "s/https:/http:/" -e "s/\/\/[^\/]*/&:8080/")	
elif [[ "$HOST" =~ ^http:.* ]] ; then
	ALT_HOST=$(echo "$HOST" | sed -e "s/http:/https:/" -e "s/:8080//")	
else
	ALT_HOST="$HOST"
fi
echo "HOST=${HOST}, ALT_HOST=${ALT_HOST}"

source /opt/app/intradb/bash-utilities/common_functions.sh

echo "Calling refresh_jsession"
HOST_SESS=$(refresh_jsession)
echo "HOST_SESS=$HOST_SESS"
IFS="," read -a HSARR <<< $HOST_SESS
JSESSIONID=${HSARR[0]}
HOST=${HSARR[1]}
IFS="$OLD_IFS"

java -Xmx4096m -Djava.awt.headless=true -cp "/opt/app/intradb/intradb-war/WEB-INF/lib/*:/opt/app/intradb/intradb-plugin-jars/*"  org.nrg.xnat.plexiviewer.converter.WebBasedQCImageCreator -host $HOST -project $PROJECT -session $SESSION -xnatId $XNATID -u $USER -pwd $PASS -raw

