#!/bin/bash

qsub -sync y -N snapshot-gen -q hcp_standard.q /nrgpackages/tools.release/intradb/singularity/run_singularity.sh /opt/app/intradb/snapshot-generation/generate-snapshots.sh $@

